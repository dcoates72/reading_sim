import numpy as np

rundir='LE_LCVA'
#sizes=np.genfromtxt('%s/sizes.txt'%rundir,delimiter=',')
#nsizes=len(sizes)
nsizes=5 # Hard-code this to 5, so that we can pre-generate all sizes (Feb 2020)

subs=1
nabs=1 #font types
nrepeats=60
#numberofrepeatsandtime 1-10:repeats and 11-20:duration
ntrials=nsizes*nabs*nrepeats

permute_entire_file=True        # Whether to randomize across aberrations
savefile="LE_LCVA.txt"      # prepends numbers for permutations to each one
num_permutes=1                 # Numer of permuations/shuffles of same image/aberration combinations: i.e., for different subjects

file_words=open("freqs.csv","rt")
words=file_words.readlines()
file_words.close()
seq=np.arange(len(words))
outfile=open("%s/%s"%(rundir,savefile),"wt")

#outfile.write ('sub,ab,size,repnum,word\n')

# For each subject...
for subn in np.arange(subs):
    # Randomize the seed for this subject (to their sub #)
    #np.random.seed(subn)
    # Permute the word list so it's random
    wordsp=np.random.permutation(words)

    # For each aberration..
    for nab in np.arange(nabs):
        # Make a list of sizes*repeats and permute it (one aber)
        seq=np.random.permutation( np.arange(nsizes*nrepeats) )
        for trial1 in np.arange(nsizes*nrepeats):
            outfile.write ('%d,%02d,%d,%d,%s\n'%(subn,nab, seq[trial1]//nrepeats,
                seq[trial1]%nrepeats, wordsp[nsizes*nrepeats*nab+trial1].strip()) )
                
print("DONE")
outfile.close()

if permute_entire_file:
    print("permuting.")
    # Read it back in
    outfile=open("%s/%s"%(rundir,savefile),"rt")
    lines=outfile.readlines()
    outfile.close()
    
    for n in range(num_permutes):
        # Shuffle it
        lines_permuted=np.random.permutation(lines)
        
        # Write it back out
        outfile=open("%s/%d_%s"%(rundir,n,savefile),"wt")
        outfile.write ('sub,ab,size,repnum,word\n') # write the header
        outfile.writelines(lines_permuted)
        outfile.close()
        print("DONE")

#outfile.write ('sub,ab,size,repnum,word\n')

