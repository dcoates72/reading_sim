import psychopy
import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
import numpy as np
import time

if __name__ == "__main__":

    done=False

    sq_state=5

    win = visual.Window(allowGUI=True, units='pix', size=(1024,768), fullscr=False,color=[-1,-1,-1])
    win.setMouseVisible(False)
    c1000=visual.Circle(win=win,radius=1000/2.0,lineColor=[1,0,0],edges=200,lineWidth=1 )
    r1000=visual.Rect(win=win,width=500, height=500,lineColor=[1,0,0],lineWidth=1 )
    fram=visual.Rect(win=win,width=1919, height=1079,lineColor=[1,0,0],lineWidth=1,closeShape=True )
    s10=visual.ShapeStim(win=win,vertices=[[-10,-10],[-10,10],[10,10],[10,-10]], lineColor=[1,0,0], closeShape=True,lineWidth=1 )
    s2=visual.ShapeStim(win=win,vertices=[[-1,-1],[-1,1],[1,1],[1,-1],[-1,-1]], lineColor=[1,0,0], closeShape=False,lineWidth=1 )
    s2.opacity=0.0

    im=np.zeros( (2,2,3)) #+1.0
    d1=visual.ImageStim(win=win, size=(2,2) ) #,texRes=1,mask="none",color=[0,0,0],size=(2,2))

    stims=[c1000,r1000,fram,d1,s10,s2]

    color_r=0.0
    color_g=0.0
    color_b=0.0

    black=[-1,-1,-1]

    while done==False:

        #noise_mask = visual.ImageStim( myWin, texRes=1, mask="none",
                        #pos=(0,0), units='pix', size=(params.size_noise,params.size_noise), colorSpace='rgb', color=[1,1,1] )


       #fullscr=params.fullscr, winType='pyglet', blendMode='add', useFBO=True, screen=params.screen )

        [astim.draw() for astim in stims]
        win.flip()
        keys=event.waitKeys()

        #myWin.blendMode='add'

        for key in keys:
            if key in [ 'escape', 'q' ]:
                #core.quit()
                done = True

            if key=='r':
                color_r=-1
            elif key=='t':
                color_r=0.0
            elif key=='y':
                color_r=1.0

            elif key=='f':
                color_g=-1.0
            elif key=='g':
                color_g=0.0
            elif key=='h':
                color_g=1.0

            elif key=='v':
                color_b=-1.0
            elif key=='b':
                color_b=0
            elif key=='n':
                color_b=1.0

            elif key=='up':
                sq_state=((sq_state+1)%6)
            elif key=='down':
                sq_state=((sq_state-1)%6)

            elif key=='1':
                d1.opacity=(d1.opacity==0)*1.0
            elif key=='2':
                s10.opacity=(s10.opacity==0)*1.0
            elif key=='3':
                r1000.opacity=(r1000.opacity==0)*1.0
            elif key=='4':
                c1000.opacity=(c1000.opacity==0)*1.0
            elif key=='5':
                fram.opacity=(fram.opacity==0)*1.0

            elif key=='p':
                fram.fillColor=color
                print "set fill", color
            elif key=='o':
                fram.fillColor=black
                

        color=np.array([color_r,color_g,color_b])

        s10.lineColor=color
        r1000.lineColor=color
        c1000.lineColor=color
        fram.lineColor=color

        if ((sq_state==1) or (sq_state==5)):
            im[0,0]=color
        else:
            im[0,0]=black

        if ((sq_state==2) or (sq_state==5)):
            im[0,1]=color
        else:
            im[0,1]=black

        if ((sq_state==3) or (sq_state==5)):
            im[1,0]=color
        else:
            im[1,0]=black

        if ((sq_state==4) or (sq_state==5)):
            im[1,1]=color
        else:
            im[1,1]=black
        d1.image=im
