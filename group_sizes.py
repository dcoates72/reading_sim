import psychopy.visual as visual
import show_paragraph
import numpy as np
import pandas as pd
from PIL import Image

# Pull out all trials of unique sizes into their own files, and rename as...
# <fname>_0 , <fname>_1 , etc...

rundir='run'

fname="N001trialRE" #cannot run a permuted one here
trials=pd.read_csv("%s/%s.txt"%(rundir,fname),delimiter=',',header=None,names=['S','ab','siz','repnum','word'])
#print( len(pd.unique( trials.size)) )
#print (( trials.siz.unique() ) )

for asiz in trials.siz.unique():
    filterd=trials.query('siz==%s'%asiz)
    fname_size='%s/%s_%d.txt'%(rundir,fname,asiz)
    filterd.to_csv(fname_size,header=True,index=False)
    print( fname_size )

print("Ok, done")
