import psychopy.visual as visual
import show_paragraph
import numpy as np
import pandas
from PIL import Image

rundir='LE_LCVA'
fname='0_LE_LCVA.txt'
words=pandas.read_csv('%s/%s'%(rundir,fname),delimiter=',')  #we need the permuted version of the file here
sizes=np.genfromtxt('%s/sizes.txt'%rundir,delimiter=',')
#sizes=[163]
ab=0

for ab in words.ab.unique():
    for nsize,size1 in enumerate(sizes):

        seqsize=nsize%5 # repeat the word sequence every 5 sizes. Any 5 consecutive sizes will be unique.

        # Get the appropriate words from the sequence
        paragraph=' '.join(words.query('(sub==0) & (size==%d) & (ab==%d)'%(seqsize,ab)).sort_values(['repnum']).word)

        # Add the info line at the end
        paragraph += '   (size=%d pix=%d)'%(nsize,size1)

        # Run the code to psychophysics code to generate 
        # Override with our specific parameters
        expt=show_paragraph.reading_expt(paragraph,max_lines=20)
        expt.height=size1
        expt.top_window=3000/2 
        expt.width_window=3000
        expt.height_window=3000
        expt.window_pos=np.array([-expt.width_window/2.0,expt.top_window])
        expt.apply_gaussian=False
        expt.apply_highlight=False
        expt.king_devick_mode=False
        expt.color_text_draw=(-1,-1,-1)
        expt.fontname='Arial'

        myWin = visual.Window( (4096,3100), allowGUI=True, units='pix', screen=0 )
        expt.setup(myWin)
        expt.draw_setup()
        expt.make_background()
        expt.background.draw()
        expt.win.getMovieFrame(buffer="back")

        # Save everything with the right name
        code='size%d_A%02d'%(nsize,ab)
        expt.win.saveMovieFrames('%s/words_%s.bmp'%(rundir,code))
        poses=np.array( [aword.pos for aword in expt.stims_words] )
        lens=np.array( [aword.len for aword in expt.stims_words] )
        np.save('%s/%s/poses_%s.npy'%(rundir,"info",code), poses )# chnage poses and length to match horizontaland vertical
        np.save('%s/%s/lens_%s.npy'%(rundir,"info",code), lens )
        myWin.close()
print("DONE")
