# -*- coding: utf-8 -*-
from psychopy import visual, core, monitors,event
import os
import numpy
import time
import numpy as np
import pandas as pd

def show1q(win,fields,num):
    message = visual.TextStim(win, color=(-1,-1,-1))
    current_answer=0
    sequence_answers=np.random.permutation(range(4)) # Create random answer order
    while True: # loop forever
        message.text=str(fields[2+num*5]) # Question
        for nans,answer in enumerate(sequence_answers):
            message.text += '\n'
            if nans==current_answer:
                message.text += "* "
            message.text += str(fields[3+num*5+answer])

        message.draw()
        win.flip()
        key=event.waitKeys()

        if key[0]=='down':
            current_answer += 1
        elif key[0]=='up':
            current_answer -= 1
        elif key[0]=='return':
            if sequence_answers[current_answer]==0:
                message.text="Correct"
                message.color=(0,1,0)
            else:
                message.text="Wrong"
                message.color=(1,0,0)
            message.draw()
            win.flip()
            core.wait(0.250)
            return (message.text=="Correct")
        current_answer %= 4

def ask3questions(which_paragraph):

    # New question display:
    answer_file=pd.read_csv("Randomization_Protocol/answer_file_filter.csv",delimiter='\t',header=None)
    answer_array=np.array(answer_file)
    idx=np.argwhere( answer_array[:,0]==which_paragraph ).squeeze()

    win = visual.Window([800,600])
    cor0 = show1q(win,answer_array[idx],0)
    cor1 = show1q(win,answer_array[idx],1)
    cor2 = show1q(win,answer_array[idx],2)
    win.close()
    return (cor0,cor1,cor2)

if __name__=="__main__":
    cor=ask3questions(238)
    print( "%d/3"% (cor[0]+cor[1]+cor[2] ) )
