from psychopy import visual, core

def get_fliptime(win,ntestsecs=5.0):
    msg  = visual.TextStim( win, 'Testing timing.\nPlease wait.', color=(-1,-1,-1 ), height=40 )

    clock=core.Clock()
    nflips=0
    while clock.getTime() < ntestsecs:
        msg.draw()
        win.flip()
        nflips += 1
    win.flip()
    return ntestsecs/nflips
