# -*- coding: utf-8 -*-
# Gaze contingent presentation based on psychopy demo script
# Nat Melnik
# C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\hw\sr_research\eyelink\eyetracker.py
# supported dev: C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\supported_config_settings.yaml
from psychopy import visual, core, monitors,event
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
import os
import numpy

import show_paragraph
import numpy as np

thresh_pixels=9999

draw_all_4=True

gaze_radius=4
bull_radius=8
center_radius=2

distance_cm=80
pix_per_cm=1/0.0355

square_sep_deg=10.0

square_sep_cm = np.tan(square_sep_deg/180.0*np.pi)*distance_cm

# The pixels locations of each of the square corners will be 1/2 of the separation,
# in relation to the center at (0,0)
square_loc_pixels = square_sep_cm * pix_per_cm / 2.0
print( square_loc_pixels)

do_calibration=True
show_gaze=False

square_time=1.0 # (sec)

square_clockwise=True # else counter-clockwise (is random desirable?)

# TODO: synchronize the looming with the time in each corner
bull_loom=False
bull_loom_rate=0.125 # sec between radius decrements by bull_loom_step pix
bull_loom_step=1 # pixels to reduce radius by

try:
    os.remove('et_data.EDF')#nm: remove edf file from the previous trial... in ideal case should save directly with another name... have not figured out yet.
except OSError:
    pass

class ExperimentRuntime(ioHubExperimentRuntime):
    def run(self,*args):
        global show_gaze

        display=self.hub.devices.display
        kb=self.hub.devices.keyboard
        mouse=self.hub.devices.mouse
        tracker=self.hub.devices.tracker

        res=display.getPixelResolution() # Current pixel resolution of the Display to be used

        monitorsetting = monitors.Monitor('default', width=39.5, distance=57)

        tracker.sendMessage("file_sample_data = GAZE, GAZERES, HREF, PUPIL, AREA, STATUS, BUTTON, INPUT")
        if do_calibration:
            tracker.runSetupProcedure()

        monsize =  [1024, 768]
        monitorsetting.setSizePix(monsize)
        window=visual.Window(monsize, units='pix', fullscr=False,allowGUI=True,color=[-1,-1,-1], screen=display.getIndex())

        window.recordFrameIntervals=False

        mouse.setSystemCursorVisibility(False)
        clock = core.Clock()

        cross_center = visual.TextStim(win=window, pos=(0, 0), text='+', alignHoriz='left', alignVert='center',
                                   height=30)
        
        gaze = visual.Circle(win=window, radius=gaze_radius, pos=(0, 0), lineWidth=0,
                                  edges=20, fillColor=[0,1,0], units='pix')

        bull = visual.Circle(win=window, radius=bull_radius, pos=(0, 0), lineWidth=0,
                                  edges=20, fillColor=[1,1,1], units='pix')
        center = visual.Circle(win=window, radius=center_radius, pos=(0, 0), lineWidth=0,
                                  edges=20, fillColor=[0,0,0], units='pix')
                                  
        photo_indicator = visual.Rect(win=window, pos=(-(monsize[0]-50)/2.0,-(monsize[1]-50)/2.0),
            width=50,height=50, fillColor=(1,1,1), units='pix')

        bulls4 = [visual.Circle(win=window, radius=bull_radius, pos=pos, lineWidth=0,
             edges=20, fillColor=[1,1,1], units='pix') for pos in (
(square_loc_pixels,-square_loc_pixels),
(-square_loc_pixels,-square_loc_pixels),
(square_loc_pixels,square_loc_pixels),
(-square_loc_pixels,square_loc_pixels) ) ]

        self.hub.clearEvents('all')
        
        tracker.sendMessage("file_sample_data = GAZE, GAZERES, HREF, PUPIL, AREA, STATUS, BUTTON, INPUT")
        
        tracker.setRecordingState(False)

        square_corner=0 # Clockwise, starting at upper right
        done=False

        window.recordFrameIntervals=True

        timer_move = core.CountdownTimer(0) # so it fires right away
        timer_loom = core.CountdownTimer(0) 
        clock.reset()
        self.hub.clearEvents('all')
        tracker.setRecordingState(True)
        tracker.sendMessage("CLAB START EXPT")
        while done==False:

            keys=event.getKeys()
                 
            if len(keys)>0:
                if 'g' in keys:
                    show_gaze=not (show_gaze)

                elif ('escape' in keys) or ('q' in keys):
                    done=True

                elif keys[0] in ['0','1','2','3','4','5','6','7','8','9']:
                    tracker.sendMessage("CLAB NUM %s"%keys[0])
                
            gpos=tracker.getPosition()
            
            if type(gpos) in [tuple,list]:
                gaze.pos = gpos

            if draw_all_4:
                [abull.draw() for abull in bulls4]

            # IF the countdown timer says time to move the bullseye:
            if timer_move.getTime()<=0:            
                timer_move.reset(square_time)
                if square_clockwise:
                    square_corner = (square_corner - 1) % 4
                else:
                    square_corner = (square_corner + 1) % 4
                if square_corner==0:
                    bull.pos=(square_loc_pixels,square_loc_pixels)
                    tracker.sendMessage("CLAB CORNER0")
                elif square_corner==1:
                    bull.pos=(-square_loc_pixels,square_loc_pixels)
                elif square_corner==2:
                    bull.pos=(-square_loc_pixels,-square_loc_pixels)
                else:
                    bull.pos=( square_loc_pixels,-square_loc_pixels)
                center.pos=bull.pos
                
                # Old mode: only show the thing when we move to a new corner
                # photo_indicator.draw()
            
                # Each time to new corner, reset the looming and clock and make max size
                timer_loom.reset(bull_loom_rate)
                bull.radius=bull_radius-1
                
            # New mode: Draw on even corners (thus will output slow square wave, not pulses)
            if square_corner%2==0:
                photo_indicator.draw()
            
            if bull_loom:
                if timer_loom.getTime()<=0:            
                    # Decrease by some units each time, keeping in range [center_radius,bull_radius]
                    bull.radius= ((bull.radius-bull_loom_step) % (bull_radius) )+ 0*center_radius
                    timer_loom.reset(bull_loom_rate)
               

            if show_gaze:
                gaze.draw()
                
            bull.draw()
            center.draw()
            
            cross_center.draw()    
            flip_time=window.flip()


        [abull.draw() for abull in bulls4]
        cross_center.draw()
        window.flip()
        window.getMovieFrame()
        window.saveMovieFrames('fixation_screenshot.png')
    
        window.recordFrameIntervals=False
        
        # After the main loops exits. Post-processing:
        tracker.setRecordingState(False)
        tracker.setConnectionState(False)

        self.hub.quit()

        # First find a new filename for the et_data and rename downloaded file
        unique_index=0
        while True:
            newname='et_data_%03d.edf'%unique_index
            try:
                os.stat(newname)
            except:
                break
                
            unique_index += 1
        os.rename('et_data.EDF', newname) # renames the EDF file
        
        window.saveFrameIntervals()
        window.close()
        
        print ("\nEyelink EDF data file name: %s\n"%newname)

        core.quit()
        
        
runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
runtime.start()

# Hint: Had to delete config files in: ~/AppData/Roaming/psychopy2
