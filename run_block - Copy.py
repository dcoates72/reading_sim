USE_EYELINK=True # Boolean input to run the eye tracker
MASK_DENSITY= 1 # Should be 1.0 normally, but set smaller when debugging (since mask generation is slow.)

import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core  
from psychopy import visual, monitors
from PIL import Image
import psychopy.gui

if USE_EYELINK:
	from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
        getCurrentDateTimeString)

import sys
import numpy as np
import time
import pdb
import random
import pandas as pd 
import fliptime
from make_mask import make_mask

gui1=psychopy.gui.Dlg()
gui1.addField("Condition:", choices=['RE_HCVA','LE_HCVA','RE_LCVA','LE_LCVA','RE_mHCVA','LE_mHCVA',])
gui1.addField("Size:", choices=[0,1,2,3,4,5,6,7,8]) #all the sizes in sizes.txt (logMAR actual)            
gui1.addField("Need to rename file:", choices=[0,1])
gui1.addField("Contrast:", choices=[1.0,0.25])           
gui1.show()
size_subject=(gui1.data[1])
rename_needed=(gui1.data[2])
rundir=gui1.data[0]
contrast_selected=float(gui1.data[3] )

#size_subject=3 #input("Please enter the number of the first size to use (-1 line from acuity) ")

#rename_needed=0 #input("Need to rename files? (0/1)")
rename_needed=(gui1.data[2])
if int(rename_needed)==1:
    import prep_size
    
    prep_size.do_rename(int(size_subject),rundir)
    


sizes=np.genfromtxt('%s/sizes.txt'%rundir,delimiter=',') #loads all possible sizes for acuity
sizes=sizes[size_subject:size_subject+5]      # This only takes five sizes desired for the subject
durations=np.genfromtxt('%s/durations.txt'%rundir,delimiter=',') # duration variable from file given
contrasts=np.genfromtxt('%s/contrasts.txt'%rundir,delimiter=',')# contrast variable from file given

default_duration_secs=10          # Default if not changing the duration otherwise
default_contrast=contrast_selected

test_variable="duration"    # "duration" or "contrast"
#durations=[0.03,0.05,0.075,0.1,0.250,0.5,1.0,2.0] # 8 durations

#durations=[0.25,1.0]        #seconds  
# contrasts=[0.5,1.0] 

allabs=range(1) # varying all aberrations, we are using unaberrated condition right now
mon = monitors.Monitor('Sujata_reading_small')  #Gamma correction
screensize=(1920,1080)
imsize=(2000,2000)

def extract_word (whichab,whichsize, whichrep,bit1=None):
        # For that condition, get the right image file and position descriptors, get new for flankers
        
        
        pos=[int(poses[whichab][whichsize][whichrep][0]),int(poses[whichab][whichsize][whichrep][1])]
        
        wordlen=int( lens[whichab][whichsize][whichrep] )
    
        if bit1 is None:
            
        
            # Now load the image on-the-fly
            img1=Image.open("%s/words_size%d_A%02d.bmp"%(rundir,whichsize,whichab))
            bit1=np.asarray(img1).mean(axis=2) #matrix with all the words
            
       
        
        
        # Use this code to make completely black (i.e., for calibration)
        bit1 *= 1
        double_word_size=4
        trim=4  # trim the edges around the word
        
        # Crop the appropriate image to extract the single words
        word_cropped=( bit1[(h//2-pos[1]-int(0.5*height)):(h//2-pos[1]-int(0.5*height)+int(height*1.1)), #vertical height
            (pos[0]+w//2-int(height/trim)):(pos[0]+w//2+wordlen-int(height/trim)+0*int(height)//7)] )# width

        word_len=np.shape(word_cropped)*double_word_size #size of the rectangle
        
        return word_cropped,word_len,bit1

def run_block(myWin,runtime):
    global poses,lens,h,w,rundir,height
    df = pd.read_csv('%s/0_%s.txt'%(rundir,rundir),delimiter=',')# change file name here permuted
    seqlist=df.values.tolist()
    
    myWin.clearBuffer()

    secs_per_flip=fliptime.get_fliptime(myWin,2.0)

    fixation1 = visual.TextStim( myWin, '+', color=(-1,-1,-1 ), height=80,pos=(0,0) ) # remove one of them , changed from | to +
    fixation2 = visual.TextStim( myWin, '+', color=(-1,-1,-1 ), height=80,pos=(0,0) )
    


    msg  = visual.TextStim( myWin, 'Loading Images.\nPlease wait.', color=(-1,-1,-1 ), height=40 )
    msg.draw()
    myWin.flip()

    #imgs= [[Image.open("sims/words_size%d_A%02d.bmp"%(n,ab)) for n in np.arange(len(sizes))] for ab in allabs ] # Image filename template to load: change to "blur"
    poses=[[np.load("%s/%s/poses_size%d_A%02d.npy"%(rundir,"info",n,ab)) for n in np.arange(len(sizes))] for ab in allabs ] #Image filename change for Horizontal and vertical
    lens= [[np.load("%s/%s/lens_size%d_A%02d.npy"%(rundir,"info",n,ab))  for n in np.arange(len(sizes))] for ab in allabs ] # change length and poses to match horizontal and vertical

    msg  = visual.TextStim( myWin, 'Making masks.\nPlease wait.', color=(-1,-1,-1 ), height=40 )
    msg.draw()
    myWin.flip()

    masks=[make_mask(win=myWin,mask_size=(512,200),letter_height=size1,
            fontname='Arial',density=MASK_DENSITY) for nsize,size1 in enumerate(sizes) ]

    img = visual.ImageStim( myWin, size=imsize) # This is the blank buffer for words display
    
    # This is a placeholder with only the first size
    imgs= [[Image.open("%s/words_size%d_A%02d.bmp"%(rundir,n,ab)) for n in np.arange(1)] for ab in [0] ] # Loads image of all the words for one size
    
    # Also average the RGB to make a grayscale img
    bits=[ [(np.asarray(img1)).mean(axis=2) for img1 in imgs[ab]] for ab in [0] ] 
    
    h,w=np.shape(bits[0][0]) # Figure out image height,width using any image (the first one)
    
    del(imgs)
    del(bits)

    # Wait with black fixation
    fixation1.draw()
    fixation2.draw()
    myWin.flip()
    keys=event.waitKeys() 

    runtime.tracker_message("START EXPERIMENT")
    runtime.tracker_recording(True)
    
    
    
    Outputfile=open("experimentwords.csv","w")
    
       
    for n in np.arange(len(seqlist)):
        
        # Show colored 'ready' fixation for msec. before word
        #fixation1.color='red'
        #fixation2.color='red'
        #fixation1.draw()
        #fixation2.draw()

        # For each line of the sequence list, pull off which ab size, and rep
        whichab=seqlist[n][1]
        whichsize=seqlist[n][2]
        whichrep=seqlist[n][3]
        
        if whichrep> 49:
            continue  #to skip if the repnum is greater in 49
        
        flankers=random.sample(range(50,59),4)
        
        message="INFO Trial %d: ab:%d size:%d rep:%d upper_flanker:%d bottom_flanker:%d lef_flanker:%d rig_flanker:%d "%(n,whichab,whichsize,whichrep,flankers[0],flankers[1],flankers[2],flankers[3]) # displaying message at the command window m
        
    
        
        runtime.tracker_message(message)
        
        height=sizes[whichsize]
        
        
        word_cropped, word_len, bit1 = extract_word (whichab,whichsize, whichrep)   #target word
        
        word_cropped_masked_top, word_len_masked_top,bit1 = extract_word (whichab,whichsize, flankers[0],bit1)  # upper flanker
        word_cropped_masked_bottom, word_len_masked_bottom,bit1 = extract_word (whichab,whichsize, flankers[1],bit1) #  bottom flanker
        word_cropped_masked_lef, word_len_masked_lef,bit1 = extract_word (whichab,whichsize, flankers[2],bit1) # left flanker
        word_cropped_masked_rig, word_len_masked_rig,bit1 = extract_word (whichab,whichsize, flankers[3],bit1) # right flanker
        

        buf_w,buf_h=imsize
        buff=np.zeros( (buf_h,buf_w))+1.0

        upr=buf_h//2-word_len[0]//2
        lef=buf_w//2-word_len[1]//2 #left side of the target word
       
        lef_flanker_xpos= lef-word_len_masked_lef[1]  #Upper left hand corner of the word for horizontal position
        rig_flanker_xpos=lef+word_len[1]
        vert_spacing_top= int (height * 1.1) # To change vertical spacing
        vert_spacing_bottom= int (height * 1.1)  
       
        buff[upr:upr+word_len[0],lef:lef+word_len[1]]=np.flipud( word_cropped/128.-1.0 ) #target word
      
        
        
        #Fankers
        
        
        buff[upr+vert_spacing_top:upr+vert_spacing_top+word_len_masked_top[0],buf_w//2-word_len_masked_top[1]//2:buf_w//2-word_len_masked_top[1]//2+word_len_masked_top[1]]=np.flipud( word_cropped_masked_top/128.-1.0 ) # upper flanker try
        buff[upr-vert_spacing_bottom:upr-vert_spacing_bottom+word_len_masked_bottom[0],buf_w//2-word_len_masked_bottom[1]//2:buf_w//2-word_len_masked_bottom[1]//2+word_len_masked_bottom[1]]=np.flipud( word_cropped_masked_bottom/128.-1.0 ) #lower flanker
        buff[upr:upr+word_len_masked_lef[0],lef_flanker_xpos:lef_flanker_xpos+word_len_masked_lef[1]]=np.flipud( word_cropped_masked_lef/128.-1.0 )  
        buff[upr:upr+word_len_masked_rig[0],rig_flanker_xpos:rig_flanker_xpos+word_len_masked_rig[1]]=np.flipud( word_cropped_masked_rig/128.-1.0 )
        
      
        
        # Draw the stimulus
        img.image=buff
        #img_flankers.image=buff
        # For durations, round up to ensure minimum time
        
        duration_flips=int(round(default_duration_secs/secs_per_flip))
        if test_variable=="duration":
            duration_idx=whichrep//10
            duration=durations[duration_idx]
            duration_flips=int(round(duration/secs_per_flip))

            img.opacity=default_contrast
            masks[whichsize].opacity=default_contrast

        elif test_variable=="contrast":
            contrast_idx=whichrep//10
            contrast=contrasts[contrast_idx]

            img.opacity=contrast
            masks[whichsize].opacity=contrast

        for n in range(duration_flips):
            img.draw()
            
            #fixation1.draw()
            #fixation2.draw()
            myWin.flip()
            
            if len( event.getKeys() ) > 0:
                break
        
        # If masked:
        masks[whichsize].draw()
        fixation1.color='black'
        fixation2.color='black'
        
        fixation1.draw()
        fixation2.draw()
       
        myWin.flip()
        core.wait(0.5) # exact timing less important here

        # Wait with black fixation
        fixation1.draw()
        fixation2.draw()
       
        myWin.flip()

        keys=event.waitKeys() 
        if ('q' in keys) or ('escape' in keys):
            break
            
       
        Outputfile.write(message +'\n')
    
      

    runtime.tracker_recording(False)
    runtime.tracker_message("END EXPERIMENT")
    
    
    Outputfile.close()
    
   

if USE_EYELINK:
    class ExperimentRuntime(ioHubExperimentRuntime):
        def tracker_message(self,msg):
            self.tracker.sendMessage(msg)
        def tracker_recording(self,on_or_off):
            self.tracker.setRecordingState(on_or_off)
            
        def run(self,*args):
            display=self.hub.devices.display
            kb=self.hub.devices.keyboard
            mouse=self.hub.devices.mouse
            tracker=self.hub.devices.tracker
            clock = core.Clock()

            mouse.setSystemCursorVisibility(False)
            self.tracker=tracker                # remember it so the virtual functions can find the tracker
            
            self.hub.clearEvents('all')
            tracker.setConnectionState(True)
            tracker.setRecordingState(False) # run_block should make True later
            clock.reset()
            self.hub.clearEvents('all')
            # This denotes the beginning of the experiment and useful especially when one is parsing the data
            tracker.runSetupProcedure()
            myWin = visual.Window( screensize, allowGUI=True, units='pix', screen=1,color=(1,1,1),monitor=mon,fullscr=False)
            run_block(myWin,self)
            tracker.setConnectionState(False) # Triggers the file copy of EDF file?
            myWin.close()
            self.hub.quit()

# Simple version to call w/o eye tracking
class NoTrackerRuntime():
    def tracker_message(self,msg):
        print msg # Debugging, mostly
        pass
    def tracker_recording(self,on_or_off):
        pass

    def start(self,*args):
        myWin = visual.Window( screensize, allowGUI=False, units='pix', screen=1,color=(1,1,1),monitor=mon,fullscr=True)
        myWin.setGamma(2.2)
        run_block(myWin,self)
        myWin.close()
       
if __name__=="__main__":
    if USE_EYELINK:
        runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
    else:
        runtime=NoTrackerRuntime()
    runtime.start()
    
