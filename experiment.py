import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core

import numpy as np
import time
import pdb

# For debugging--not sure if we need this in general!
import matplotlib.pyplot as plt

color_text=(-0.5,-0.5,-0.5)

height=50
total_width=500

myWin = visual.Window((total_width,height*1.0), allowGUI=True, units='pix', color=(0.0,0.0,0.0) )
myWin.setMouseVisible(False)
#myWin.setRecordFrameIntervals(True)

color_text_calc=(1,1,1)
pos_left=(-total_width/2.0,height/2.0)
text = visual.TextStim(myWin,pos=pos_left,alignHoriz='left', alignVert='top', height=height, color=color_text_calc,text="Hello there")
#text.setText("Hello there, how are you?")

def zero_search(arr,startat):
# startat allows skip for for startat letters
    idx=np.max( np.arange(len(arr[startat:]))[arr[startat:]>0] )+startat
    return idx

def process_win():
    buf2=myWin._getFrame("back")
    bw=np.mean(np.array(buf2),2)
    vertical_sum=np.sum(bw,0)
    vertical_sum = vertical_sum - np.min(vertical_sum)
    vertical_sum = vertical_sum / np.max(vertical_sum)
    vertical_sum = vertical_sum * height # + height
    return bw,vertical_sum,zero_search(vertical_sum,0)

#buf1=myWin._getFrame("front")
#buf3=myWin._getFrame("front")
#buf4=myWin._getFrame("back")
#event.waitKeys()
#myWin.close()
#bw=


text.setText("X")
text.draw()
bw,vertical_sum,end_X=process_win()
myWin.clearBuffer()
text.setText("HelloX")
text.draw()
bw,vertical_sum,word_end=process_win()
myWin.clearBuffer()
text.setText("Hello thereX")
text.draw()
bw,vertical_sum,word2_X=process_win()

plt.xlim(0,total_width)
#plt.imshow(bw)

#plt.subplot(2,1,1)

plt.plot( vertical_sum )

word_end=zero_search(vertical_sum,10)

plt.plot( [word_end]*2,[0,10], 'r--' )

#plt.subplot(2,1,2)
myWin.close()
plt.grid()
plt.show()
