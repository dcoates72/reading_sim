import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
from PIL import Image

import sys
import numpy as np
import time
import pdb
import random
import pandas as pd
from psychopy import visual, monitors

#sizes=[14,20,24,30,40,50,60,70]
#sizes=10**np.linspace(np.log10(10),np.log10(80),8)
sizes=[10,20,30,40,50,60,70,80]
ab=0
#mon = monitors.Monitor('sujata5 ')
screensize=(1024,768)
imsize=(1024,768)

if __name__=="__main__":
    df = pd.read_csv('sequence_S1.txt',delimiter=',')
    seqlist=df.query("ab==%2d"%ab).values.tolist()

    myWin = visual.Window( screensize, allowGUI=True, units='pix', screen=0,color=(1,1,1))#fullscr=True)
    fixation = visual.TextStim( myWin, '+', color=(-1,-1,-1 ), height=40 )

    img = visual.ImageStim( myWin, size=screensize)
    
    imgs=[Image.open("sims/words_size%d_A%02d.bmp"%(n,ab)) for n in np.arange(8)]


    # Draw the stimulus
    img.image=img[0]
    img.draw()
    myWin.flip() 
    
    # wait wiyh stim onscreen
    keys=event.waitKeys() 
    
    # Wait for #150
    core.wait(0.40)
    
    
    # Erases the stimulus
    #myWin.flip() 
    
    # Wait with black fixation
    fixation.color='black'
    fixation.draw()
    myWin.flip()
    
#    keys=event.waitKeys() 
#    if ('q' in keys) or ('escape' in keys):
#        break
    
    # Show red fixation for 1/2 sec. before word
    fixation.color='red'
    fixation.draw()
    myWin.flip()
    core.wait(1.0)
    
    myWin.close()
