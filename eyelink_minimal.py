from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)

tracker=self.hub.devices.tracker

        self.hub.clearEvents('all')
        tracker.setRecordingState(False)

        #This sends the different messages regarding the experimental conditions to the eye tracker which is then used to 
        #write in the final EDF file. 
        tracker.sendMessage("CLAB Paragraph_number=%d max_lines=%d"%(self.paragraph_number,max_lines))
        tracker.sendMessage("CLAB subject=%s session_num=%d trial_num=%d"%(self.subject,self.session_num,self.trial_num))
        tracker.sendMessage("CLAB eye mode=%s"%self.eye_mode)


        clock.reset()
        self.hub.clearEvents('all')
        tracker.setRecordingState(True)
        #This denotes the beginning of the experiment and useful especially when one is parsing the data
        tracker.sendMessage("CLAB START EXPT")


        # After the trial loops exits:
        window.recordFrameIntervals=False
        tracker.setRecordingState(False)
        tracker.setConnectionState(False)

 
        # First find a new filename for the et_data and rename downloaded file
        # Do this for each trial
        unique_index=0
        while True:
            newname='reading_et_data_%s_%d_T%d_%03d.edf'%(self.subject,
                    self.session_num,self.trial_num,unique_index)
            word_file_name='reading_word_data_%s_%d_T%d_%03d.txt'%(self.subject,
                    self.session_num,self.trial_num,unique_index)
            try:
                os.stat(newname)
                
            except:
                break
                
            unique_index += 1
        os.rename('et_data.EDF',newname) # renames the EDF file


        self.hub.quit()
        window.saveFrameIntervals()
        window.close()
        
        print ("\nEyelink EDF data file name: %s\n"%newname)