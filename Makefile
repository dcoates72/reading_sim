# Get all the .png files in sims/ and convert
# them to appropriate BMPs
BMPS = $( $(wildcard sims/*.png):.png=.bmp)

all: sequence images bmps.zip 

sequence:
	python3 gen_seq.py > asequence.txt

images:
	python2 gen_ims.py 

run:
	python2 run_block.py

%.bmp: %.png
	convert  $< -colorspace gray -depth 8 bmp3:$@

bmps.zip: $(BMPS)
	zip bmps.zip $(BMPS)
