import os

def do_rename(num_first,rundir='run'):
    if num_first>0:
        for n in range(5):
            fname_src = "%s/words_size%d_A00.bmp"%(rundir,num_first+n)
            fname_dst = "%s/words_size%d_A00.bmp"%(rundir,n)
            try:
                os.remove(fname_dst)
            except:
                pass # ok if already has been removed
            os.rename(fname_src,fname_dst)
            print ( '%s -> %s' % (fname_src, fname_dst ) )

            fname_src = "%s/info/lens_size%d_A00.npy"%(rundir,num_first+n)
            fname_dst = "%s/info/lens_size%d_A00.npy"%(rundir,n)
            try:
                os.remove(fname_dst)
            except:
                pass # ok if already has been removed
            os.rename(fname_src,fname_dst)
            print ( '%s -> %s' % (fname_src, fname_dst ) )

            fname_src = "%s/info/poses_size%d_A00.npy"%(rundir,num_first+n)
            fname_dst = "%s/info/poses_size%d_A00.npy"%(rundir,n)
            try:
                os.remove(fname_dst)
            except:
                pass # ok if already has been removed
            os.rename(fname_src,fname_dst)
            print ( '%s -> %s' % (fname_src, fname_dst ) )

            print ("") # blank_line
    else:
        n=4
        
    # Remove everything else
    print (n)
    while n<15:
        n += 1
        fname_src = "%s/words_size%d_A00.bmp"%(rundir,n)
        try:
            os.remove(fname_src)
        except:
            continue # no problem, keep trying
        fname_src = "%s/info/lens_size%d_A00.npy"%(rundir,n)
        os.remove(fname_src)
        fname_src = "%s/info/poses_size%d_A00.npy"%(rundir,n)
        os.remove(fname_src)
        print ("Removed 3 files for %d"%n)

if __name__=="__main__":
    num_first=input("Please enter the number of the first size to use (-1 line from acuity) ")
    do_rename(num_first)
