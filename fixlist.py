import pandas as pd

fname="LE_LCVA/0_LE_LCVA%s.txt"
start_size=1

df=pd.read_csv(fname%"")
 
            
fixed_words=[df.query('size==%d & repnum==%d'%((row[1][2]+start_size)%5,row[1][3])).word.values[0]  for row in df.iterrows()] #only replace words in the repnum

df["word"]=fixed_words
df_filtered = df.query("repnum<50")
print(df_filtered)


#df_filtered.to_csv(fname%"_fixed",index=False)
df.to_csv(fname%"_fixed",index=False)
print("DONE")
